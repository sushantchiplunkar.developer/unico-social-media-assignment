import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:unico_assignment_flutter/constants.dart';
import 'package:unico_assignment_flutter/models/message.dart';

class ChatService extends ChangeNotifier {
  //Send Messages
  Future<void> sendMessages(String receiverId, String message) async {
    final String currentUserId = firebaseAuth.currentUser!.uid;
    final String currentUserEmail = firebaseAuth.currentUser!.email.toString();
    final Timestamp timestamp = Timestamp.now();

    Message newMessage = Message(
        senderId: currentUserId,
        receiverId: receiverId,
        senderEmail: currentUserEmail,
        message: message,
        timestamp: timestamp);

    List<String> ids = [currentUserId, receiverId];
    ids.sort();
    String chatRoomId = ids.join("_");

    await fireStore
        .collection('chat_rooms')
        .doc(chatRoomId)
        .collection('messages')
        .add(newMessage.toJson());
  }

//Receive Messages
  Stream<QuerySnapshot> getMessages(String userId, String secondPersonId) {
    List<String> ids = [userId, secondPersonId];
    ids.sort();
    String chatRoomId = ids.join("_");

    return fireStore
        .collection('chat_rooms')
        .doc(chatRoomId)
        .collection('messages')
        .orderBy('timestamp', descending: false)
        .snapshots();
  }
}
