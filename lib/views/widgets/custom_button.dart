import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String label;
  final Color bgColor;
  final Color labelColor;
  final bool isEnable;
  final void Function() buttonClick;

  const CustomButton(
      {super.key,
      required this.label,
      this.bgColor = const Color(0xFF007DFA),
      this.isEnable = true,
      this.labelColor = Colors.white,
      required this.buttonClick});

  @override
  Widget build(BuildContext context) {
    return isEnable
        ? InkWell(
            onTap: buttonClick,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 52,
              //padding: const EdgeInsets.all(16),
              decoration: ShapeDecoration(
                color: bgColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
              child: Center(
                child: Text(
                  label,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: labelColor,
                    fontSize: 18,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          )
        : Container(
            width: MediaQuery.of(context).size.width,
            height: 52,
            //padding: const EdgeInsets.all(16),
            decoration: ShapeDecoration(
              color: Colors.grey,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
            child: Center(
              child: Text(
                label,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontSize: 18,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          );
  }
}
