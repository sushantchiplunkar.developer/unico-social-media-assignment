import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:unico_assignment_flutter/constants.dart';
import 'package:unico_assignment_flutter/views/screens/signup_screen.dart';
import 'package:unico_assignment_flutter/views/widgets/custom_button.dart';
import 'package:unico_assignment_flutter/views/widgets/text_input_field.dart';

class SignInScreen extends StatelessWidget {
  SignInScreen({super.key});

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  void loginClick() {
    log('login working fine');
  }

  void signUpClick() {
    log('signup working fine');
    Get.to(()=>SignupScreen());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Login ',
              style: TextStyle(
                  fontSize: 56,
                  color: textColor.withOpacity(0.5),
                  fontWeight: FontWeight.w900),
            ),
            const SizedBox(
              height: 35,
            ),
            TextInputField(
                controller: _emailController,
                labelText: 'Email',
                icon: Icons.email),
            const SizedBox(
              height: 25,
            ),
            TextInputField(
              controller: _passwordController,
              labelText: 'Password',
              icon: Icons.lock,
              isObscure: true,
            ),
            const SizedBox(
              height: 35,
            ),
            CustomButton(
              label: 'Login Now',
              buttonClick: () => signInSignUpController.signInUser(
                  _emailController.text, _passwordController.text),
            ),
            const SizedBox(
              height: 25,
            ),
            InkWell(
              onTap: signUpClick,
              child: const Row(
                children: [
                  Text(
                    'Do not Have An Account?',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Sign up',
                    style: TextStyle(
                      fontSize: 25,
                      color: Color(0xFF007DFA),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
