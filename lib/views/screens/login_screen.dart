import 'package:flutter/material.dart';
import 'package:unico_assignment_flutter/constants.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Login ',
              style: TextStyle(
                  fontSize: 56,
                  color: textColor.withOpacity(0.5),
                  fontWeight: FontWeight.w900),
            ),
            Form(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  const Text('Email Address'),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    textCapitalization: TextCapitalization.none,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      ),
                      hintText: 'Enter a email address',
                      prefixIcon: Icon(Icons.email_outlined),
                    ),
                    validator: (value) {
                      if (value == null ||
                          value.trim().isEmpty ||
                          !value.contains('@')) {
                        return 'Please enter the valid email address.';
                      }

                      return null;
                    },
                    // onSaved: (newValue) {
                    //   _enterEmail = newValue!;
                    // },
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const Text('Password'),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    //obscureText: !_passwordVisibility,
                    obscuringCharacter: '*',
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      ),
                      hintText: 'Enter password',
                      prefixIcon: Icon(Icons.password_outlined),
                      // suffixIcon: IconButton(
                      //   onPressed: () {
                      //     // setState(() {
                      //     //   _passwordVisibility = !_passwordVisibility;
                      //     // });
                      //   },
                      //   icon: Icon(_passwordVisibility
                      //       ? Icons.visibility_off_rounded
                      //       : Icons.visibility_rounded),
                      // ),
                    ),
                    validator: (value) {
                      if (value == null ||
                          value.trim().isEmpty ||
                          value.length < 6) {
                        return 'Password must be at least 6 characters long';
                      }
                      return null;
                    },
                    // onSaved: (newValue) {
                    //   _enterPassword = newValue!;
                    // },
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  // if (_validationIsInProcess)
                  //   const Center(
                  //     child: CircularProgressIndicator(),
                  //   ),
                  // if (!_validationIsInProcess)
                    InkWell(
                      //onTap: _submit,
                      child: Container(
                        width: 500,
                        height: 52,
                        padding: const EdgeInsets.all(16),
                        decoration: ShapeDecoration(
                          color: const Color(0xFF007DFA),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                        ),
                        child: const Text(
                          'Login now',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                  const SizedBox(
                    height: 30,
                  ),
                  InkWell(
                    onTap: () {
                      // setState(() {
                      //   Navigator.pushNamed(context, SignUp.routeName);
                      // });
                    },
                    child: const Row(
                      children: [
                        Text('Do not Have An Account?'),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Sign up',
                          style: TextStyle(
                            color: Color(0xFF007DFA),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
