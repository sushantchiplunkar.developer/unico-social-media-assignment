import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:unico_assignment_flutter/constants.dart';
import 'package:unico_assignment_flutter/views/screens/chat_screen.dart';
import 'package:unico_assignment_flutter/views/screens/message_screen.dart';
import 'package:unico_assignment_flutter/views/screens/profile_screen.dart';
import 'package:unico_assignment_flutter/views/screens/search_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int pageIndex = 0;

  List screens = [const HomePage(), const MessageScreen(), const SearchScreen(), ProfileScreen()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        onTap: (index) {
          setState(() {
            pageIndex = index;
          });
        },
        type: BottomNavigationBarType.fixed,
        backgroundColor: bgColor,
        selectedItemColor: Colors.blue,
        currentIndex: pageIndex,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              size: 30,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.message,
              size: 30,
            ),
            label: 'Message',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.search,
              size: 30,
            ),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
              size: 30,
            ),
            label: 'Profile',
          )
        ],
      ),
      body: screens[pageIndex],
      // body: Center(
      //   child: Row(
      //     mainAxisAlignment: MainAxisAlignment.center,
      //     children: [
      //       const Text('Home Screen'),
      //       const SizedBox(
      //         width: 10,
      //       ),
      //       IconButton(
      //         onPressed: () {
      //           log('logout');
      //           signInSignUpController.signOut();
      //         },
      //         icon: const Icon(Icons.logout),
      //       ),
      //     ],
      //   ),
      // ),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text('Home',style: TextStyle(color: bgColor),),
          backgroundColor: buttonColor,
          actions: [
            IconButton(
              onPressed: () {
                log('logout');
                signInSignUpController.signOut();
              },
              icon: const Icon(Icons.logout,color: bgColor,),
            )
          ],
        ),
        body: const Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Home Screen'),
              SizedBox(
                width: 10,
              ),
            ],
          ),
        ));
  }


}
