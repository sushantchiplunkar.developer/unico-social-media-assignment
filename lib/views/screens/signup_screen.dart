import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:unico_assignment_flutter/constants.dart';
import 'package:unico_assignment_flutter/views/widgets/custom_button.dart';
import 'package:unico_assignment_flutter/views/widgets/text_input_field.dart';

class SignupScreen extends StatelessWidget {
  SignupScreen({super.key});

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _fullNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  void loginClick() {
    log('login working fine');
  }

  void uploadProfileClick() {}

  void signUpClick() {
    log('signup working fine');
    if (_fullNameController.text.isNotEmpty &&
        _emailController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty) {
      signInSignUpController.registerNewUser(
          _fullNameController.text,
          _emailController.text,
          _passwordController.text,
          signInSignUpController.profilePic);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              height: 35,
            ),
            Text(
              'Sign Up ',
              style: TextStyle(
                  fontSize: 56,
                  color: textColor.withOpacity(0.5),
                  fontWeight: FontWeight.w900),
            ),
            const SizedBox(
              height: 35,
            ),
            Stack(
              children: [
                const CircleAvatar(
                  radius: 64,
                  backgroundColor: Colors.white,
                  backgroundImage: NetworkImage(
                      'https://www.pngitem.com/pimgs/m/150-1503945_transparent-user-png-default-user-image-png-png.png'),
                ),
                Positioned(
                  bottom: -10,
                  left: 80,
                  child: IconButton(
                    icon: const Icon(Icons.add_a_photo),
                    onPressed: () async =>
                        signInSignUpController.getImageOrPickFromGallery(),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 25,
            ),
            TextInputField(
                controller: _fullNameController,
                labelText: 'Full Name',
                icon: Icons.person),
            const SizedBox(
              height: 25,
            ),
            TextInputField(
                controller: _emailController,
                labelText: 'Email',
                icon: Icons.email),
            const SizedBox(
              height: 25,
            ),
            TextInputField(
              controller: _passwordController,
              labelText: 'Password',
              icon: Icons.lock,
              isObscure: true,
            ),
            const SizedBox(
              height: 35,
            ),
            CustomButton(
                label: 'Register Now', buttonClick: () => signUpClick()),
            const SizedBox(
              height: 25,
            ),
            InkWell(
              onTap: loginClick,
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Already Have An Account?',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Login In',
                    style: TextStyle(
                      fontSize: 25,
                      color: Color(0xFF007DFA),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
