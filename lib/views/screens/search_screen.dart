import 'package:flutter/material.dart';
import 'package:unico_assignment_flutter/constants.dart';

class SearchScreen extends StatelessWidget {
  const SearchScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Search',style: TextStyle(color: bgColor),),
        backgroundColor: buttonColor,
      ),
      body: Center(
        child: Text('Search Screen'),
      ),
    );
  }
}
