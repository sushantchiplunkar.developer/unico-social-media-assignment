import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:unico_assignment_flutter/constants.dart';
import 'package:unico_assignment_flutter/models/user.dart';
import 'package:unico_assignment_flutter/views/widgets/custom_button.dart';

import '../widgets/text_input_field.dart';

class ProfileScreen extends StatelessWidget {
  ProfileScreen({super.key});

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _fullNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final Rx<bool> isEditing = false.obs;

  Future<User> getUser(String email) async {
    try {
      CollectionReference users =
      fireStore.collection('users');
      final snapshot = await users.doc(email).get();
      //final data = snapshot.data() as Map<String, dynamic>;
      User user = User.fromSnap(snapshot);
      return user;
    } catch (e) {
      Get.snackbar("error", e.toString());
      throw(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    _emailController.text = firebaseAuth.currentUser!.email.toString();
    getUser(firebaseAuth.currentUser!.uid).then((value) => _fullNameController.text = value.name);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        actions: [
          IconButton(
            tooltip: 'Edit Profile',
            onPressed: () {
              isEditing.value = !isEditing.value;
            },
            icon: const Icon(Icons.settings, color: bgColor),
          )
        ],
        title: const Text(
          'Profile',
          style: TextStyle(color: bgColor),
        ),
        backgroundColor: buttonColor,
      ),
      body: profilePage(),
      // body: const Center(
      //   child: Text('Profile Screen'),
      // ),
    );
  }

  Widget _buildUserList() {
    return StreamBuilder<QuerySnapshot>(
      stream: fireStore.collection('users').snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return const Text('error');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Text('loading..');
        }


        return ListView(
          children: snapshot.data!.docs
              .map<Widget>((doc) => _buildUserListItem(doc))
              .toList(),
        );
      },
    );
  }

  Widget _buildUserListItem(DocumentSnapshot documentSnapshot) {
    Map<String, dynamic> data =
    documentSnapshot.data()! as Map<String, dynamic>;

    if (firebaseAuth.currentUser!.email != data['email']) {
      return profilePage();
    }else{
      return Container();
    }
  }

  Widget profilePage() {
    
    return SingleChildScrollView(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(
            height: 35,
          ),
          Stack(
            children: [
              const CircleAvatar(
                radius: 64,
                backgroundColor: Colors.white,
                backgroundImage: NetworkImage(
                    'https://www.pngitem.com/pimgs/m/150-1503945_transparent-user-png-default-user-image-png-png.png'),
              ),
              Positioned(
                bottom: -10,
                left: 80,
                child: IconButton(
                  icon: const Icon(Icons.add_a_photo),
                  onPressed: () async =>
                      signInSignUpController.getImageOrPickFromGallery(),
                ),
              )
            ],
          ),
          const SizedBox(
            height: 25,
          ),
          TextInputField(
              controller: _fullNameController,
              labelText: 'Full Name',
              icon: Icons.person),
          const SizedBox(
            height: 25,
          ),
          TextInputField(
              controller: _emailController,
              labelText: 'Email',
              icon: Icons.email),
          const SizedBox(
            height: 25,
          ),
          // TextInputField(
          //   controller: _passwordController,
          //   labelText: 'Password',
          //   icon: Icons.lock,
          //   isObscure: true,
          // ),
          const SizedBox(
            height: 35,
          ),
          Obx(
            () => CustomButton(
              label: 'Update Profile',
              isEnable: isEditing.value,
              buttonClick: () {},
            ),
          ),
          const SizedBox(
            height: 25,
          ),
          /*InkWell(
            onTap: loginClick,
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Already Have An Account?',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Login In',
                  style: TextStyle(
                    fontSize: 25,
                    color: Color(0xFF007DFA),
                  ),
                )
              ],
            ),
          )*/
        ],
      ),
    );
  }
}
