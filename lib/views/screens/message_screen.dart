import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:unico_assignment_flutter/constants.dart';
import 'package:unico_assignment_flutter/views/screens/chat_screen.dart';

class MessageScreen extends StatelessWidget {
  const MessageScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Messages',style: TextStyle(color: bgColor),),
        backgroundColor: buttonColor,
      ),
      body: _buildUserList(),
    );

  }

  Widget _buildUserList() {
    return StreamBuilder<QuerySnapshot>(
      stream: fireStore.collection('users').snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return const Text('error');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Text('loading..');
        }

        return ListView(
          children: snapshot.data!.docs
              .map<Widget>((doc) => _buildUserListItem(doc))
              .toList(),
        );
      },
    );
  }

  Widget _buildUserListItem(DocumentSnapshot documentSnapshot) {
    Map<String, dynamic> data =
    documentSnapshot.data()! as Map<String, dynamic>;

    if (firebaseAuth.currentUser!.email != data['email']) {
      return ListTile(
        title: Text(data['email']),
        onTap: () {
          Get.to(
                () => ChatScreen(
              receiverUserEmail: data['email'],
              receiverUserId: data['uid'],
            ),
          );
        },
      );
    }else{
      return Container();
    }
  }
}
