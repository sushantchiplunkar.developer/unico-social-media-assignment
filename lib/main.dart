import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:unico_assignment_flutter/controllers/sigup_signin_controller.dart';
import 'package:unico_assignment_flutter/firebase_options.dart';
import 'package:unico_assignment_flutter/views/screens/signin_screen.dart';
import 'package:unico_assignment_flutter/views/screens/signup_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  ).then((value) {
    Get.put(SignInSignUpController());
  });
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home:SignInScreen(),
      //home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

