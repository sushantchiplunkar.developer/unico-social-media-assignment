
import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:unico_assignment_flutter/constants.dart';

class ProfileController extends GetxController{

  final Rx<String> _uid = "".obs;

  updateUserId(String uid){
    _uid.value = uid;
    getUserInfo();
  }
  
  getUserInfo() async{
    DocumentSnapshot userInfoDoc = await fireStore.collection('users').doc(_uid.value).get();
    final userInfo = userInfoDoc.data()! as dynamic;
    String name = userInfo['name'];
    String profilePhoto = userInfo['profilePhoto'];
  }
}