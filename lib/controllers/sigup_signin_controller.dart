import 'dart:developer';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:unico_assignment_flutter/constants.dart';
import 'package:unico_assignment_flutter/models/user.dart' as model;
import 'package:unico_assignment_flutter/views/screens/home_screen.dart';
import 'package:unico_assignment_flutter/views/screens/signin_screen.dart';

class SignInSignUpController extends GetxController {
  static SignInSignUpController instance = Get.find();
  late Rx<File?> _galleryImagePicked;
  late Rx<User?> _userData;

  File? get profilePic => _galleryImagePicked.value;

  Future<String> _uploadImageToFireStore(File profilePic) async {
    Reference reference = firebaseStorage
        .ref('user')
        .child('profilePics')
        .child(firebaseAuth.currentUser!.uid);

    UploadTask uploadTask = reference.putFile(profilePic);
    TaskSnapshot snapshot = await uploadTask;
    String downloadUrl = await snapshot.ref.getDownloadURL();

    return downloadUrl ?? 'No Url found! Sorry try again Later';
  }

  /*Future<String> _uploadDataToFireStore(Object data) async{


  }*/

  @override
  void onReady() {
    super.onReady();
    _userData = Rx<User?>(firebaseAuth.currentUser);
    _userData.bindStream(firebaseAuth.authStateChanges());
    ever(_userData, _setInitialScreen);
  }

  _setInitialScreen(User? user){
    if(user!=null){
      Get.offAll(()=>const HomeScreen());
    }else{
      Get.offAll(()=> SignInScreen());
    }
  }

  void signInUser(String emailId, String password) async {
    try{
      if(emailId.isNotEmpty && password.isNotEmpty){
        await firebaseAuth.signInWithEmailAndPassword(email: emailId, password: password);
        log('login success');
      }else{
         Get.snackbar('Error Logging in', 'Please enter all the fields');
            //Get.showSnackbar();
      }
    }catch(e){
      Get.snackbar('Error Logging in', e.toString());
    }
  }

  void registerNewUser(
      String fullName, String emailId, String password, File? image) async {
    try {
      if (fullName.isNotEmpty &&
          emailId.isNotEmpty &&
          password.isNotEmpty &&
          image != null) {
        UserCredential userCredential = await firebaseAuth
            .createUserWithEmailAndPassword(email: emailId, password: password);
        String downloadUrl = await _uploadImageToFireStore(image);
        model.User user = model.User(
          name: fullName,
          email: emailId,
          uid: userCredential.user!.uid,
          profilePhoto: downloadUrl,
        );
        firebaseAuth.currentUser!.displayName.obs.value = fullName;
        await fireStore.collection('users').doc(userCredential.user!.uid).set(user.toJson());
      }else{
        Get.snackbar('Register New User Error', "Please enter all the fields values");
      }
    } catch (e) {
      Get.snackbar('Register New User Error', e.toString());
    }
  }

  void getImageOrPickFromGallery() async {
    final pickedImage = await ImagePicker().pickImage(source: ImageSource.gallery);
    if(pickedImage!=null){
      //Get.snackbar('Profile Picture', 'You have successfully selected your profile picture!').show();
      log('You have successfully selected your profile picture!');
    }
    _galleryImagePicked = Rx<File?>(File(pickedImage!.path));
  }

  void signOut() async{
    await firebaseAuth.signOut();
  }
}
