import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:unico_assignment_flutter/controllers/sigup_signin_controller.dart';

//colors 
const bgColor = Colors.white;
var buttonColor = Colors.blue;
const borderColor = Colors.grey;
const textColor = Colors.black;

//firebase constants
var firebaseAuth = FirebaseAuth.instance;
var firebaseStorage = FirebaseStorage.instance;
var fireStore = FirebaseFirestore.instance;

//controller constants
var signInSignUpController = SignInSignUpController.instance;