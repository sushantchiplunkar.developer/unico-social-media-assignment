
import 'package:cloud_firestore/cloud_firestore.dart';

class Message{
  final String senderId;
  final String receiverId;
  final String senderEmail;
  //final String receiverEmail;
  final String message;
  final Timestamp timestamp;

  Message({required this.senderId,
    required this.receiverId,
    required this.senderEmail,
    //required this.receiverEmail,
    required this.message,
    required this.timestamp});

  Map<String,dynamic> toJson()=>{
    'senderId':senderId,
    'senderEmail':senderEmail,
    'receiverId': receiverId,
    //'receiverEmail':receiverEmail,
    'message':message,
    'timestamp':timestamp
  };
}